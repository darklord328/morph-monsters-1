@echo off

REM autobuild for Morph Classes

REM check if the directories exist

set /P version=Insert version for PK3: 
goto check

:missing
echo Something is missing! :(
pause
exit

:check
if not exist .\acc\acc.exe goto missing
if not exist .\7za\7za.exe goto missing
if not exist .\pk3data\ goto missing

REM compile ACS

ECHO Compiling ACS...
mkdir .\pk3data\acs
acc\acc.exe pk3data\acssrc\unmorph pk3data\acs\global

REM packaging

echo Zipping to the PK3 file...
7za\7za.exe a -tzip morphmonstersandaddons-v%version%.pk3 .\pk3data\*

REM remove ACS object file so next time this is ran there won't be 2 object files

del /Q .\pk3data\acs

echo Done!
pause
